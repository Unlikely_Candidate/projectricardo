// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FirearmCameraRecoilComponent.generated.h"


USTRUCT(BlueprintType)
struct RICARDO_API FFirearmCameraRecoilSettings
{
	GENERATED_USTRUCT_BODY()

	FFirearmCameraRecoilSettings()
		: Precision(0.001f)
		, RecoveryDelay(0.0f)
		, RecoverSpeedVertical(1.0f)
		, RecoverSpeedHorizontal(1.0f)
		, RiseRateVertical(1.0f)
		, RiseRateHorizontal(1.0f)
		, MinTimeBetweenShots(0.2f)
	{
	}

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FRuntimeFloatCurve RecoilByTimeHorizontal;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FRuntimeFloatCurve RecoilByTimeVertical;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Precision;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float RecoveryDelay;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (DisplayName = "Recover Speed Vertical (deg/sec)"))
	float RecoverSpeedVertical;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (DisplayName = "Recover Speed Horizontal (deg/sec)"))
	float RecoverSpeedHorizontal;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float RiseRateVertical;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float RiseRateHorizontal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	float MinTimeBetweenShots;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RICARDO_API UFirearmCameraRecoilComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFirearmCameraRecoilComponent();

public:
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "ApplyShot", ScriptName = "ApplyShot"))
	void BP_ApplyShot();
	void ApplyShot();

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "ApplyCameraPitchInput", ScriptName = "ApplyCameraPitchInput"))
	float BP_ApplyCameraPitchInput(float PitchInput);
	float ApplyCameraPitchInput(float PitchInput);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "ApplyCameraYawInput", ScriptName = "ApplyCameraYawInput"))
	float BP_ApplyCameraYawInput(float YawInput);
	float ApplyCameraYawInput(float YawInput);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "GetRecoil", ScriptName = "GetRecoil"))
	FRotator BP_GetRecoil() const;
	FRotator GetRecoil() const;

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "GetRecoilDelta", ScriptName = "GetRecoilDelta"))
	FRotator BP_GetRecoilDelta() const;
	FRotator GetRecoilDelta() const;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FFirearmCameraRecoilSettings Settings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	float RiseTimeSpeed;

	float GetRecoilHorizontal() const;
	float GetRecoilVertical() const;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	static float SmoothStep(float DeltaTime, float Current, float Wished, float RecoverRate, float RiseRate);
	static float GetCompensationAngle(float Current, float Change);

	float GetTimeByAngle(float Time, float AngleChange, const FRichCurve* RecoilByTime, bool BackwardOnly = false) const;

	float RecoilTimeHorizontal;
	float RecoilTimeVertical;

	float LastShotTime;

	FRotator RecoilAngles;
	FRotator RecoilAnglesPrevFrame;
};
