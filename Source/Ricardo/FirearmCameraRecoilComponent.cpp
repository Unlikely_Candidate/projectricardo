// Fill out your copyright notice in the Description page of Project Settings.


#include "FirearmCameraRecoilComponent.h"

#include "Engine.h"

//DEFINE_LOG_CATEGORY_STATIC(LogCameraRecoil, Verbose, All);

// Sets default values for this component's properties
UFirearmCameraRecoilComponent::UFirearmCameraRecoilComponent()
	: RiseTimeSpeed(1.0f)
	, RecoilTimeHorizontal(0.0f)
	, RecoilTimeVertical(0.0f)
	, LastShotTime(0.0f)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UFirearmCameraRecoilComponent::BP_ApplyShot()
{
	ApplyShot();
}


void UFirearmCameraRecoilComponent::ApplyShot()
{
	const float ElapsedTimeLevel = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	const float Time = FMath::Min(ElapsedTimeLevel - LastShotTime, Settings.MinTimeBetweenShots) * RiseTimeSpeed;

	LastShotTime = ElapsedTimeLevel;

	RecoilTimeHorizontal += Time;
	RecoilTimeVertical += Time;
}


float UFirearmCameraRecoilComponent::BP_ApplyCameraPitchInput(float PitchInput)
{
	return ApplyCameraPitchInput(PitchInput);
}


float UFirearmCameraRecoilComponent::ApplyCameraPitchInput(float PitchInput)
{
	float CompensationX = GetCompensationAngle(GetRecoilVertical(), PitchInput);
	if (FMath::Abs(CompensationX) > KINDA_SMALL_NUMBER) {
		RecoilTimeVertical = GetTimeByAngle(RecoilTimeVertical, FMath::Abs(CompensationX), Settings.RecoilByTimeVertical.GetRichCurveConst(), true);
	}

	return PitchInput - CompensationX;
}


float UFirearmCameraRecoilComponent::BP_ApplyCameraYawInput(float YawInput)
{
	return ApplyCameraYawInput(YawInput);
}


float UFirearmCameraRecoilComponent::ApplyCameraYawInput(float YawInput)
{
	float CompensationY = GetCompensationAngle(GetRecoilHorizontal(), YawInput);
	if (FMath::Abs(CompensationY) > KINDA_SMALL_NUMBER) {
		RecoilTimeHorizontal = GetTimeByAngle(RecoilTimeHorizontal, FMath::Abs(CompensationY), Settings.RecoilByTimeHorizontal.GetRichCurveConst(), true);
	}

	return YawInput - CompensationY;
}


FRotator UFirearmCameraRecoilComponent::BP_GetRecoil() const
{
	return GetRecoil();
}


FRotator UFirearmCameraRecoilComponent::GetRecoil() const
{
	return RecoilAngles;
}


FRotator UFirearmCameraRecoilComponent::BP_GetRecoilDelta() const
{
	return GetRecoilDelta();
}


FRotator UFirearmCameraRecoilComponent::GetRecoilDelta() const
{
	const FRotator Delta = RecoilAngles - RecoilAnglesPrevFrame;
	return Delta;
}


float UFirearmCameraRecoilComponent::SmoothStep(float DeltaTime, float Current, float Wished, float RecoverRate, float RiseRate)
{
	float Sign = FMath::Sign(Wished - Current);
	float RateLog = Sign < 0.0f ? RecoverRate : RiseRate;
	float Rate = FMath::Exp2(RateLog);

	return FMath::Lerp(Wished, Current, FMath::Exp2(-Rate * DeltaTime));
}


float UFirearmCameraRecoilComponent::GetCompensationAngle(float Current, float Change)
{
	if (FMath::Sign(Change) != FMath::Sign(Current)) {
		return 0.0f;
	}

	return FMath::Sign(Change) * FMath::Min(FMath::Abs(Current), FMath::Abs(Change));
}


float UFirearmCameraRecoilComponent::GetTimeByAngle(float Time, float AngleChange, const FRichCurve* RecoilByTime, bool BackwardOnly /*= false*/) const
{
	if (!RecoilByTime)
	{
		return 0.0f;
	}

	if (Time <= KINDA_SMALL_NUMBER)
	{
		return 0.0f;
	}

	const float Step = BackwardOnly || (FMath::Abs(RecoilByTime->Eval(Time)) >= FMath::Abs(RecoilByTime->Eval(Time - KINDA_SMALL_NUMBER))) ? -Settings.Precision : Settings.Precision;
	
	float PreviousTime = Time;
	float PreviousAngle = RecoilByTime->Eval(Time);
	float PreviousAngleChange = AngleChange;

	while (PreviousAngle > KINDA_SMALL_NUMBER)
	{
		const float CurrentTime = FMath::Max(Step + PreviousTime, 0.0f);
		const float CurrentAngle = RecoilByTime->Eval(CurrentTime);
		const float CurrentAngleChange = PreviousAngleChange + (FMath::Abs(CurrentAngle) - FMath::Abs(PreviousAngle));

		// Maybe local minimum
		if (!BackwardOnly && CurrentAngleChange > PreviousAngleChange)
		{
			return PreviousTime;
		}

		// Passing zero line
		if (PreviousAngle * CurrentAngle <= 0.0f)
		{
			return 0.0f;
		}

		if (CurrentAngleChange <= 0.0)
		{
			return FMath::GetMappedRangeValueClamped(FVector2D(PreviousAngleChange, CurrentAngleChange), FVector2D(PreviousTime, CurrentTime), 0.0f);
		}

		PreviousTime = CurrentTime;
		PreviousAngle = CurrentAngle;
		PreviousAngleChange = CurrentAngleChange;
	}

	return 0.0f;
}


float UFirearmCameraRecoilComponent::GetRecoilHorizontal() const
{
	return Settings.RecoilByTimeHorizontal.GetRichCurveConst()->Eval(RecoilTimeHorizontal);
}


float UFirearmCameraRecoilComponent::GetRecoilVertical() const
{
	return Settings.RecoilByTimeVertical.GetRichCurveConst()->Eval(RecoilTimeVertical);
}


// Called every frame
void UFirearmCameraRecoilComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	const float ElapsedTimeLevel = UGameplayStatics::GetRealTimeSeconds(GetWorld());

	if (ElapsedTimeLevel > LastShotTime + Settings.RecoveryDelay)
	{
		RecoilTimeVertical = GetTimeByAngle(RecoilTimeVertical, Settings.RecoverSpeedVertical * DeltaTime, Settings.RecoilByTimeVertical.GetRichCurveConst(), true);
		RecoilTimeHorizontal = GetTimeByAngle(RecoilTimeHorizontal, Settings.RecoverSpeedHorizontal * DeltaTime, Settings.RecoilByTimeHorizontal.GetRichCurveConst(), true);
	}

	RecoilAnglesPrevFrame = RecoilAngles;

	constexpr float RecoverRate = 10.0f;

	RecoilAngles.Pitch = SmoothStep(DeltaTime, RecoilAngles.Pitch, GetRecoilVertical(), RecoverRate, Settings.RiseRateVertical);
	RecoilAngles.Yaw = SmoothStep(DeltaTime, RecoilAngles.Yaw, GetRecoilHorizontal(), RecoverRate, Settings.RiseRateHorizontal);
}
