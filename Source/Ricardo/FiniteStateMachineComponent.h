// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FiniteStateMachineState.h"
#include "FiniteStateMachineComponent.generated.h"


UCLASS(Blueprintable, Abstract, EditInlineNew)
class RICARDO_API UFiniteStateMachine : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Instanced, Category = "FSM")
	TArray<UFSM_State*> States;
};


DECLARE_DYNAMIC_MULTICAST_SPARSE_DELEGATE_OneParam(FFSM_OnEnterStateSignature, UFiniteStateMachineComponent, OnEnterState, FString, StateName);
DECLARE_DYNAMIC_MULTICAST_SPARSE_DELEGATE_OneParam(FFSM_OnExitStateSignature, UFiniteStateMachineComponent, OnExitState, FString, StateName);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RICARDO_API UFiniteStateMachineComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFiniteStateMachineComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable)
	FFSM_OnEnterStateSignature OnEnterState;

	UPROPERTY(BlueprintAssignable)
	FFSM_OnExitStateSignature OnExitState;

#if WITH_EDITORONLY_DATA
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FSM")
	bool IsDebugOnscreenDrawEnabled;
#endif // WITH_EDITORONLY_DATA

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FSM", meta = (DisplayName = "FSM"))
	TSubclassOf<UFiniteStateMachine> FiniteStateMachineClass;

private:
	UFUNCTION()
	void ProcessNextState(const FString& StateName);

	void PreprocessStates();
	void DrawDebugOnscreenInfo() const;
	bool IsValidFiniteStateMachine(const TArray<UFSM_State*>& StatesToValidate) const;

	UPROPERTY()
	UFiniteStateMachine* FiniteStateMachine;

	TMap<FString, UFSM_State*> States;
	UFSM_State*	CurrentState;
};
