// Fill out your copyright notice in the Description page of Project Settings.


#include "FiniteStateMachineComponent.h"
#include "Engine.h"


DEFINE_LOG_CATEGORY_STATIC(LogFSM, Verbose, All);


// Sets default values for this component's properties
UFiniteStateMachineComponent::UFiniteStateMachineComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UFiniteStateMachineComponent::BeginPlay()
{
	Super::BeginPlay();

	PreprocessStates();

	const FString HomeStateName = TEXT("Idle");

	if (!States.Find(HomeStateName))
	{
		UE_LOG(LogFSM, Error, TEXT("Failed to find FSM home state"));
		return;
	}

	ProcessNextState(HomeStateName);
}


// Called every frame
void UFiniteStateMachineComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

#if WITH_EDITORONLY_DATA
	if (IsDebugOnscreenDrawEnabled)
	{
		DrawDebugOnscreenInfo();
	}
#endif // WITH_EDITORONLY_DATA
}


void UFiniteStateMachineComponent::ProcessNextState(const FString& StateName)
{
	if (CurrentState)
	{
		OnExitState.Broadcast(CurrentState->Name);
	}

	UFSM_State* NextState = States.FindRef(StateName);

	if (!NextState)
	{
		UE_LOG(LogFSM, Error, TEXT("Failed to find state with name: %f"), *StateName);
		return;
	}

	CurrentState = NextState;
	CurrentState->Enter();
	CurrentState->OnExit.RemoveAll(this);
	CurrentState->OnExit.AddDynamic(this, &UFiniteStateMachineComponent::ProcessNextState);

	OnEnterState.Broadcast(StateName);
}


void UFiniteStateMachineComponent::PreprocessStates()
{
	if (!FiniteStateMachineClass)
	{
		UE_LOG(LogFSM, Error, TEXT("Invalid FSM asset FiniteStateMachineComponent"));
		return;
	}

	FiniteStateMachine = NewObject<UFiniteStateMachine>(this, FiniteStateMachineClass);

	if (!IsValidFiniteStateMachine(FiniteStateMachine->States))
	{
		UE_LOG(LogFSM, Error, TEXT("Invalid FSM setup in FiniteStateMachineComponent"));
		return;
	}

	for (UFSM_State* State : FiniteStateMachine->States)
	{
		State->Init(GetOwner());
		States.Add(State->Name, State);
	}
}

bool UFiniteStateMachineComponent::IsValidFiniteStateMachine(const TArray<UFSM_State*>& StatesToValidate) const
{
	if (StatesToValidate.Num() <= 0.0)
	{
		UE_LOG(LogFSM, Error, TEXT("Zero states"));
		return false;
	}

	for (int32 Index = 0; Index < StatesToValidate.Num(); Index++)
	{
		if (!StatesToValidate[Index])
		{
			UE_LOG(LogFSM, Error, TEXT("Invalid state"));
			return false;
		}

		// Search for duplicated states
		for (int32 Sub = 0; Sub < StatesToValidate.Num(); Sub++)
		{
			if (Sub != Index && StatesToValidate[Index]->Name == StatesToValidate[Sub]->Name)
			{
				UE_LOG(LogFSM, Error, TEXT("Duplicated state: %s"), *StatesToValidate[Sub]->Name);
				return false;
			}
		}

		// Validate states. Check if it is referencing to existing state
		for (UFSM_Link* Link : StatesToValidate[Index]->Links)
		{
			if (!Link)
			{
				UE_LOG(LogFSM, Error, TEXT("Invalid link inside state: %s"), *StatesToValidate[Index]->Name)
				continue;
			}

			bool IsRefStateFound = false;

			for (int32 J = 0; J < StatesToValidate.Num(); ++J)
			{
				if (StatesToValidate[J]->Name == Link->NextStateName)
				{
					IsRefStateFound = true;
					break;
				}
			}

			if (!IsRefStateFound)
			{
				UE_LOG(LogFSM, Error, TEXT("Link of the state %s referensing to unexisted state %s"), *StatesToValidate[Index]->Name, *Link->NextStateName);
			}
		}
	}

	return true;
}

void UFiniteStateMachineComponent::DrawDebugOnscreenInfo() const
{
	FString Msg = "--- Finite State Machine ---\n";

	for (auto Iter = States.CreateConstIterator(); Iter; ++Iter)
	{
		const UFSM_State* State = (*Iter).Value;
		Msg.Append(TEXT("-") + State->Name);
		
		if (CurrentState && CurrentState->Name == State->Name)
		{
			Msg.Append(TEXT("(ON)"));
		}

		Msg.Append(TEXT("\n"));
	}

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(0, 1.0f, FColor::White, Msg);
	}
}
