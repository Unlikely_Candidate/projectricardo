#include "FiniteStateMachineState.h"
#include "FiniteStateMachineCondition.h"
#include "Engine.h"


DEFINE_LOG_CATEGORY_STATIC(LogFSM, Verbose, All);


void UFSM_Link::Initialize(AActor* Actor)
{
	Owner = Actor;

	for (UFSM_Condition* Condition : Conditions)
	{
		Condition->Initialize(Owner);
	}
}


bool UFSM_Link::IsSatisfied() const
{
	ensureMsgf(Owner != NULL, TEXT("Using unitialized fsm link"));

	for (UFSM_Condition* Condition : Conditions)
	{
		if (!Condition->GetResult(Owner))
		{
			return false;
		}
	}

	return true;
}


void UFSM_State::Init(AActor* OwningActor)
{
	Owner = OwningActor;

	for (UFSM_Link* Link : Links)
	{
		Link->Initialize(Owner);
	}
}


void UFSM_State::Enter()
{
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();

	ensureMsgf(!TimerManager.IsTimerActive(TickHandle), TEXT("State tick timer should be stopped"));
	constexpr float Rate = 1.0 / 60.0f; // each frame
	TimerManager.SetTimer(TickHandle, this, &UFSM_State::Tick, Rate, true);
}


void UFSM_State::Exit(const FString& NextStateName)
{
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.ClearTimer(TickHandle);

	OnExit.Broadcast(NextStateName);
}


void UFSM_State::Tick()
{
	for (UFSM_Link* Link : Links)
	{
		if (Link->IsSatisfied())
		{
			Exit(Link->NextStateName);
			break;
		}
	}
}

