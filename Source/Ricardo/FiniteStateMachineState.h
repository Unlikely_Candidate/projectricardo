#pragma once

#include "CoreMinimal.h"
#include "FiniteStateMachineState.generated.h"

class UFSM_Condition;

UCLASS(Blueprintable, BlueprintType, EditInlineNew, DefaultToInstanced)
class RICARDO_API UFSM_Link : public UObject
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFSM_OnConditionSucceed, const FString&, NextState);

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString NextStateName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Instanced)
	TArray<UFSM_Condition*>	Conditions;

	void Initialize(AActor* Actor);
	bool IsSatisfied() const;

private:
	AActor*	Owner;
};


UCLASS(Blueprintable, BlueprintType, EditInlineNew, DefaultToInstanced)
class RICARDO_API UFSM_State : public UObject
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFSM_OnStateExit, const FString&, NextStateName);

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Instanced)
	TArray<UFSM_Link*> Links;

	FFSM_OnStateExit OnExit;

	void Init(AActor* OwningActor);
	void Enter();
	UFUNCTION()
	void Exit(const FString& NextStateName);

private:
	void Tick();

	FTimerHandle TickHandle;
	AActor*	Owner;
};