#pragma once

#include "CoreMinimal.h"
#include "FiniteStateMachineCondition.generated.h"


UCLASS(Blueprintable, BlueprintType, EditInlineNew, DefaultToInstanced, Abstract)
class RICARDO_API UFSM_Condition : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "OnInit"))
	void ReceiveInit(AActor* Owner);

	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "OnReturn"))
	void ReceiveResult(AActor* Actor, bool &Value);

	virtual void Initialize(AActor* Owner) { ReceiveInit(Owner); };
	virtual bool GetResult(AActor* Actor);
};


UCLASS(BlueprintType, EditInlineNew, DefaultToInstanced)
class RICARDO_API UFSM_ConditionInputActionPressed : public UFSM_Condition
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName ActionName;

	virtual void Initialize(AActor* Owner)	override;
	virtual bool GetResult(AActor* Actor) override { return IsPressed; }

private:
	void HandlePress() { IsPressed = true; };
	void HandleRelease() { IsPressed = false; }

	APawn* OwnerPawn;
	FInputActionBinding	Binding;
	bool IsPressed;
};


UCLASS(BlueprintType, EditInlineNew, DefaultToInstanced)
class RICARDO_API UFSM_ConditionInputActionReleased : public UFSM_Condition
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName ActionName;

	virtual void Initialize(AActor* Owner)	override;
	virtual bool GetResult(AActor* Actor) override { return IsReleased; }

private:
	void HandlePress() { IsReleased = false; };
	void HandleRelease() { IsReleased = true; };

	APawn* OwnerPawn;
	FInputActionBinding	Binding;
	bool IsReleased;
};
