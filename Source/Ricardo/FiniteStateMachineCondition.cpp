#include "FiniteStateMachineCondition.h"


DEFINE_LOG_CATEGORY_STATIC(LogFSM, Verbose, All);


bool UFSM_Condition::GetResult(AActor* Actor)
{
	bool Result;
	ReceiveResult(Actor, Result);
	return Result;
}


void UFSM_ConditionInputActionPressed::Initialize(AActor* Owner)
{
	Super::Initialize(Owner);

	OwnerPawn = Cast<APawn>(Owner);

	if (!OwnerPawn)
	{
		return;
	}

	OwnerPawn->InputComponent->BindAction(ActionName, EInputEvent::IE_Pressed, this, &UFSM_ConditionInputActionPressed::HandlePress);
	OwnerPawn->InputComponent->BindAction(ActionName, EInputEvent::IE_Released, this, &UFSM_ConditionInputActionPressed::HandleRelease);
}


void UFSM_ConditionInputActionReleased::Initialize(AActor* Owner)
{
	Super::Initialize(Owner);

	OwnerPawn = Cast<APawn>(Owner);

	if (!OwnerPawn)
	{
		return;
	}

	OwnerPawn->InputComponent->BindAction(ActionName, EInputEvent::IE_Pressed, this, &UFSM_ConditionInputActionReleased::HandlePress);
	OwnerPawn->InputComponent->BindAction(ActionName, EInputEvent::IE_Released, this, &UFSM_ConditionInputActionReleased::HandleRelease);
}
